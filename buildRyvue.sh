#!/bin/bash
cd /opt/projects/RuoYi-Vue_KC01/
git pull
cd ruoyi-ui
npm run build:prod
cp -r dist/* docker/html/dist/
cd docker
docker build -t ryvue-ui:1 .
docker stop ryvue-ui
docker run -d --rm -p 8888:80 --network=ryvuentw --name ryvue-ui ryvue-ui:1