import request from '@/utils/request'

// 查询日志列表
export function listDaylog(query) {
  return request({
    url: '/system/daylog/list',
    method: 'get',
    params: query
  })
}

// 查询日志详细
export function getDaylog(logId) {
  return request({
    url: '/system/daylog/' + logId,
    method: 'get'
  })
}

// 新增日志
export function addDaylog(data) {
  return request({
    url: '/system/daylog',
    method: 'post',
    data: data
  })
}

// 修改日志
export function updateDaylog(data) {
  return request({
    url: '/system/daylog',
    method: 'put',
    data: data
  })
}

// 删除日志
export function delDaylog(logId) {
  return request({
    url: '/system/daylog/' + logId,
    method: 'delete'
  })
}
