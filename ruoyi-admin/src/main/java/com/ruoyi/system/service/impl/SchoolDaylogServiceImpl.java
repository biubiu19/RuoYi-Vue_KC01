package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.SchoolDaylogMapper;
import com.ruoyi.system.domain.SchoolDaylog;
import com.ruoyi.system.service.ISchoolDaylogService;

/**
 * 日志Service业务层处理
 * 
 * @author Jules
 * @date 2024-04-23
 */
@Service
public class SchoolDaylogServiceImpl implements ISchoolDaylogService 
{
    @Autowired
    private SchoolDaylogMapper schoolDaylogMapper;

    /**
     * 查询日志
     * 
     * @param logId 日志主键
     * @return 日志
     */
    @Override
    public SchoolDaylog selectSchoolDaylogByLogId(Long logId)
    {
        return schoolDaylogMapper.selectSchoolDaylogByLogId(logId);
    }

    /**
     * 查询日志列表
     * 
     * @param schoolDaylog 日志
     * @return 日志
     */
    @Override
    public List<SchoolDaylog> selectSchoolDaylogList(SchoolDaylog schoolDaylog)
    {
        return schoolDaylogMapper.selectSchoolDaylogList(schoolDaylog);
    }

    /**
     * 新增日志
     * 
     * @param schoolDaylog 日志
     * @return 结果
     */
    @Override
    public int insertSchoolDaylog(SchoolDaylog schoolDaylog)
    {
        schoolDaylog.setCreateTime(DateUtils.getNowDate());
        return schoolDaylogMapper.insertSchoolDaylog(schoolDaylog);
    }

    /**
     * 修改日志
     * 
     * @param schoolDaylog 日志
     * @return 结果
     */
    @Override
    public int updateSchoolDaylog(SchoolDaylog schoolDaylog)
    {
        return schoolDaylogMapper.updateSchoolDaylog(schoolDaylog);
    }

    /**
     * 批量删除日志
     * 
     * @param logIds 需要删除的日志主键
     * @return 结果
     */
    @Override
    public int deleteSchoolDaylogByLogIds(Long[] logIds)
    {
        return schoolDaylogMapper.deleteSchoolDaylogByLogIds(logIds);
    }

    /**
     * 删除日志信息
     * 
     * @param logId 日志主键
     * @return 结果
     */
    @Override
    public int deleteSchoolDaylogByLogId(Long logId)
    {
        return schoolDaylogMapper.deleteSchoolDaylogByLogId(logId);
    }
}
