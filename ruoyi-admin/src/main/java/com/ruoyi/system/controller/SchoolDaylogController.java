package com.ruoyi.system.controller;

import java.util.List;
import java.util.concurrent.TimeUnit;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.system.util.RedisStatUtil;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.SchoolDaylog;
import com.ruoyi.system.service.ISchoolDaylogService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 日志Controller
 * 
 * @author Jules
 * @date 2024-04-23
 */
@RestController
@RequestMapping("/system/daylog")
public class SchoolDaylogController extends BaseController
{
    @Autowired
    private ISchoolDaylogService schoolDaylogService;

    @Autowired
    private RedisStatUtil redisStatUtil;

    @Autowired
    private RedisTemplate redisTemplate;

    /**
     * 获取日志详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:daylog:query')")
    @GetMapping(value = "/{logId}")
    public AjaxResult getInfo(@PathVariable("logId") Long logId)
    {
        String key = redisStatUtil.REDIS_KEY_DAYLOG_NAME+logId;
        SchoolDaylog schoolDaylog;
        //判断redis中是否存在缓存
        if (redisStatUtil.isExist(key)){
            schoolDaylog = (SchoolDaylog)redisTemplate.opsForValue().get(key);
        }else {//不存在缓存,则去数据库中拿
            schoolDaylog = schoolDaylogService.selectSchoolDaylogByLogId(logId);

            //并且拿出来以后,放入redis中,设置过期时间为5小时(避免太久的数据)
            redisTemplate.opsForValue().set(key,schoolDaylog,5, TimeUnit.HOURS);
        }
        return success(schoolDaylog);
    }


    /**
     * 查询日志列表
     */
    @PreAuthorize("@ss.hasPermi('system:daylog:list')")
    @GetMapping("/list")
    public TableDataInfo list(SchoolDaylog schoolDaylog)
    {
        startPage();
        List<SchoolDaylog> list = schoolDaylogService.selectSchoolDaylogList(schoolDaylog);
        return getDataTable(list);
    }

    /**
     * 导出日志列表
     */
    @PreAuthorize("@ss.hasPermi('system:daylog:export')")
    @Log(title = "日志", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, SchoolDaylog schoolDaylog)
    {
        List<SchoolDaylog> list = schoolDaylogService.selectSchoolDaylogList(schoolDaylog);
        ExcelUtil<SchoolDaylog> util = new ExcelUtil<SchoolDaylog>(SchoolDaylog.class);
        util.exportExcel(response, list, "日志数据");
    }



    /**
     * 新增日志
     */
    @PreAuthorize("@ss.hasPermi('system:daylog:add')")
    @Log(title = "日志", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SchoolDaylog schoolDaylog)
    {
        return toAjax(schoolDaylogService.insertSchoolDaylog(schoolDaylog));
    }

    /**
     * 修改日志
     */
    @PreAuthorize("@ss.hasPermi('system:daylog:edit')")
    @Log(title = "日志", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SchoolDaylog schoolDaylog)
    {
        //删除redis中的缓存,保持redis中最新缓存和mysql实际值一样
        redisStatUtil.delKey(redisStatUtil.REDIS_KEY_DAYLOG_NAME+schoolDaylog.getLogId());
        return toAjax(schoolDaylogService.updateSchoolDaylog(schoolDaylog));
    }

    /**
     * 删除日志
     */
    @PreAuthorize("@ss.hasPermi('system:daylog:remove')")
    @Log(title = "日志", businessType = BusinessType.DELETE)
	@DeleteMapping("/{logIds}")
    public AjaxResult remove(@PathVariable Long[] logIds)
    {
        return toAjax(schoolDaylogService.deleteSchoolDaylogByLogIds(logIds));
    }

    /**
     * 判断redis中是否存在key
     * @param key redis的键名
     * @return 是否存在的结果
     */
    public boolean isExist(String key){
        return redisTemplate.hasKey(key);
    }

}
