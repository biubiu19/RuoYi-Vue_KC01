package com.ruoyi.system.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.ListOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Component;

@Component
public class RedisStatUtil{

    public static final String REDIS_KEY_STUDENT_NAME = "daylog:updatelog:key";
    public static final String REDIS_KEY_DAYLOG_NAME = "daylog:getlog:key";

    public static final String REDIS_KEY_DAYLOG_UPLOG = "daylog:updatelog:key";
    public static final String REDIS_KEY_DAYLOG_DELETE_ID = "daylog:delete:id:key";
    public static final String REDIS_KEY_DAYLOG_INSTAL_ID = "daylog:addlog:key";


    @Autowired
    private RedisTemplate redisTemplate;

    /**
     * 判断redis中是否存在key
     * @param key redis的键名
     * @return 是否存在的结果
     */
    public boolean isExist(String key) {
        return redisTemplate.hasKey(key);
    }

    private void operationsValue() {
        //得到redis中操作String的工具类(如果可以确定返回类型,可以在这里规定好泛型),并且添加了一条数据
        ValueOperations<String, String> valueOperations = redisTemplate.opsForValue();
        valueOperations.set(REDIS_KEY_STUDENT_NAME, "zhangsan");

        //得到redis中操作List的工具类
        ListOperations listOperations = redisTemplate.opsForList();
        listOperations.leftPushAll("likes", "man", "zuqiu", "java", "lanqiu");
    }

    public void delKey(String s) {
        redisTemplate.delete(s);
    }
}
