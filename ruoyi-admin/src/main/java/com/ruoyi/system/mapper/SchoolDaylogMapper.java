package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.SchoolDaylog;

/**
 * 日志Mapper接口
 * 
 * @author Jules
 * @date 2024-04-23
 */
public interface SchoolDaylogMapper 
{
    /**
     * 查询日志
     * 
     * @param logId 日志主键
     * @return 日志
     */
    public SchoolDaylog selectSchoolDaylogByLogId(Long logId);

    /**
     * 查询日志列表
     * 
     * @param schoolDaylog 日志
     * @return 日志集合
     */
    public List<SchoolDaylog> selectSchoolDaylogList(SchoolDaylog schoolDaylog);

    /**
     * 新增日志
     * 
     * @param schoolDaylog 日志
     * @return 结果
     */
    public int insertSchoolDaylog(SchoolDaylog schoolDaylog);

    /**
     * 修改日志
     * 
     * @param schoolDaylog 日志
     * @return 结果
     */
    public int updateSchoolDaylog(SchoolDaylog schoolDaylog);

    /**
     * 删除日志
     * 
     * @param logId 日志主键
     * @return 结果
     */
    public int deleteSchoolDaylogByLogId(Long logId);

    /**
     * 批量删除日志
     * 
     * @param logIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSchoolDaylogByLogIds(Long[] logIds);
}
