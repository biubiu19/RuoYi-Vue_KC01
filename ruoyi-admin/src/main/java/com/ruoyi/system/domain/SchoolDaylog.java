package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 日志对象 school_daylog
 * 
 * @author Jules
 * @date 2024-04-23
 */
public class SchoolDaylog extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long logId;

    /** 班级ID */
    @Excel(name = "班级ID")
    private Long classId;

    /** 班级名 */
    @Excel(name = "班级名")
    private String className;

    /** 学生ID */
    @Excel(name = "学生ID")
    private Long studentId;

    /** 学生姓名 */
    @Excel(name = "学生姓名")
    private String studentName;

    /** 上课速度 */
    @Excel(name = "上课速度")
    private Long lessonSpeed;

    /** 上课质量(0未处理,1.已处理) */
    @Excel(name = "上课质量(0未处理,1.已处理)")
    private Long lessonScore;

    /** 排序字段 */
    @Excel(name = "排序字段")
    private Long orderNum;

    /** 日志内容 */
    @Excel(name = "日志内容")
    private String logContent;

    /** 上课建议 */
    @Excel(name = "上课建议")
    private String logAdvice;

    /** 日志状态 */
    @Excel(name = "日志状态")
    private Integer logStatus;

    /** 日志评语 */
    @Excel(name = "日志评语")
    private String logRemark;



    /** 日志评级（评分） */
    @Excel(name = "日志评级", readConverterExp = "评=分")
    private String logScore;

    public void setLogId(Long logId) 
    {
        this.logId = logId;
    }

    public Long getLogId() 
    {
        return logId;
    }
    public void setClassId(Long classId) 
    {
        this.classId = classId;
    }

    public Long getClassId() 
    {
        return classId;
    }
    public void setClassName(String className) 
    {
        this.className = className;
    }

    public String getClassName() 
    {
        return className;
    }
    public void setStudentId(Long studentId) 
    {
        this.studentId = studentId;
    }

    public Long getStudentId() 
    {
        return studentId;
    }
    public void setStudentName(String studentName) 
    {
        this.studentName = studentName;
    }

    public String getStudentName() 
    {
        return studentName;
    }
    public void setLessonSpeed(Long lessonSpeed) 
    {
        this.lessonSpeed = lessonSpeed;
    }

    public Long getLessonSpeed() 
    {
        return lessonSpeed;
    }
    public void setLessonScore(Long lessonScore) 
    {
        this.lessonScore = lessonScore;
    }

    public Long getLessonScore() 
    {
        return lessonScore;
    }
    public void setOrderNum(Long orderNum) 
    {
        this.orderNum = orderNum;
    }

    public Long getOrderNum() 
    {
        return orderNum;
    }
    public void setLogContent(String logContent) 
    {
        this.logContent = logContent;
    }

    public String getLogContent() 
    {
        return logContent;
    }
    public void setLogAdvice(String logAdvice) 
    {
        this.logAdvice = logAdvice;
    }

    public String getLogAdvice() 
    {
        return logAdvice;
    }
    public void setLogStatus(Integer logStatus) 
    {
        this.logStatus = logStatus;
    }

    public Integer getLogStatus() 
    {
        return logStatus;
    }
    public void setLogRemark(String logRemark) 
    {
        this.logRemark = logRemark;
    }

    public String getLogRemark() 
    {
        return logRemark;
    }
    public void setLogScore(String logScore) 
    {
        this.logScore = logScore;
    }

    public String getLogScore() 
    {
        return logScore;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("logId", getLogId())
            .append("classId", getClassId())
            .append("className", getClassName())
            .append("studentId", getStudentId())
            .append("studentName", getStudentName())
            .append("lessonSpeed", getLessonSpeed())
            .append("createTime", getCreateTime())
            .append("lessonScore", getLessonScore())
            .append("orderNum", getOrderNum())
            .append("logContent", getLogContent())
            .append("logAdvice", getLogAdvice())
            .append("logStatus", getLogStatus())
            .append("logRemark", getLogRemark())
            .append("logScore", getLogScore())
            .toString();
    }
}
